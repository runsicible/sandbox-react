import React, { useEffect } from 'react'

export const testHOC = props => {
    const { Component } = props
    console.log('hubo un cambio en props')
    

    const newProps = {...props, name: Component.name}   
    return () => <Component {...newProps}  />
}