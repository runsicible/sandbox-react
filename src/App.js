import React from 'react'
import './App.css'
import { withReducer, withProps, compose } from 'recompose'
import { ContainerButtons_ } from './containerButtons'
import { testHOC } from './testHOC'

const testReducer = ( count, action ) => {
  switch (action.type) {
    case 'INCREMENT':
      return count + 1
      case 'DECREMENT':
        return count - 1
        default:
          return count
    }
  }
      
const enhance = withReducer('counter', 'dispatch', testReducer, 0 )
const App = enhance(({ counter, dispatch }) => {
 
 const ComposedHoc = testHOC({ Component: ContainerButtons_,  dispatch })
  return (
    <div className="App">
      <header>
        this is a sandbox app 
      </header>
      <pre>el valor del counter es {counter}</pre>
      <ComposedHoc />
    </div>
  );
})

export default App;
