import React from 'react'
import { Button } from '@material-ui/core'

export const ContainerButtons_ = props => {
    const { dispatch, name } = props
    return (
        <>
        <pre>el componente se llama {name}</pre>
        <Button
            onClick={() => dispatch({ type: 'INCREMENT' })}
            color='secondary'
            variant='contained'>
            increment
        </Button>
            <Button
            onClick={() => dispatch({ type: 'DECREMENT' })}
            color='secondary'
            variant='contained'>
            decrement
        </Button>
        </>
    )

}

